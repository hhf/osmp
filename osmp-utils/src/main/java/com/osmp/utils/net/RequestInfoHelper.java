/*   
 * Project: OSMP
 * FileName: RequestInfoHelper.java
 * version: V1.0
 */
package com.osmp.utils.net;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestInfoHelper {
	private static final Logger logger = LoggerFactory
			.getLogger(RequestInfoHelper.class);
    
	/**
	 * 获取http请求远程客户端ip地址
	 * 
	 * @param request
	 * @return
	 */
    public final static String getRemoteIp(HttpServletRequest request){
        String ip = request.getHeader("X-Forwarded-For");   
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))   
            ip = request.getHeader("Proxy-Client-IP");   
  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))   
            ip = request.getHeader("WL-Proxy-Client-IP");   
  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))   
            ip = request.getHeader("HTTP_CLIENT_IP");   
  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))   
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");   
  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))   
            ip = request.getRemoteAddr();   
  
        if ("0:0:0:0:0:0:0:1".equals(ip.trim()))   
            ip = "server";   
  
        return ip;   
    }

	/**
	 * 多IP处理，可以得到最终ip
	 * @return
	 */
	public final static String getLocalIp() {
		String localip = null;// 本地IP，如果没有配置外网IP则返回它
		String netip = null;// 外网IP
		try {
			Enumeration<NetworkInterface> netInterfaces = NetworkInterface
					.getNetworkInterfaces();
			InetAddress ip = null;
			boolean finded = false;// 是否找到外网IP
			while (netInterfaces.hasMoreElements() && !finded) {
				NetworkInterface ni = netInterfaces.nextElement();
				Enumeration<InetAddress> address = ni.getInetAddresses();
				while (address.hasMoreElements()) {
					ip = address.nextElement();
					if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress()
							&& ip.getHostAddress().indexOf(":") == -1) {// 外网IP
						netip = ip.getHostAddress();
						finded = true;
						break;
					} else if (ip.isSiteLocalAddress()
							&& !ip.isLoopbackAddress()
							&& ip.getHostAddress().indexOf(":") == -1) {// 内网IP
						localip = ip.getHostAddress();
					}
				}
			}
		} catch (SocketException e) {
			logger.error("get local ip error", e);
		}
		if (netip != null && !"".equals(netip)) {
			return netip;
		} else {
			return localip;
		}
	}
}
