/*   
 * Project: OSMP
 * FileName: ServiceInterceptor.java
 * version: V1.0
 */
package com.osmp.intf.define.interceptor;

/**
 * Description:拦截器
 * 所有基于osmp的拦截器都需要实现该接口，拦截器按作用域分为全局拦截器，BUNDLE拦截器，SERVICE拦截器三种类型。
 *        全局拦截器(ALL)：      作用于osmp的所有BUNDLE和服务。
 *   BUNDLE拦截器(BUNDLE)：     作用于osmp指定BUNDLE下的所有服务。
 * SERVICE拦截器(SERVICE)：     作用于osmp指定的SERVICE服务。
 * 
 * 拦截器适用于 接口安全、权限校验、全局日志、调用链日志Track、特定服务AOP拦截等业务场景
 * 
 * 服务发布时，需要在service-properties 里指定拦截器的类型（type）和匹配的拦截策略(pattern)
 * 
 * <osgi:service interface="com.osmp.intf.define.interceptor.ServiceInterceptor" ref="osmp.demo.interceptor">
		<osgi:service-properties>
			<entry key="name" value="Interceptor_demo" />
			<entry key="mark" value="拦截器demo"/>
			<entry key="type" value="SERVICE"/> //type：ALL、BUNDLE、SERVICE
			<entry key="pattern" value="这里是发布的服务名称"/>  //支持正则匹配、   AAAService*  、  *AAA* 、   
		</osgi:service-properties>
	</osgi:service>
 * 
 * 拦截器与bundle、服务两种绑定方式：
 * 1、拦截器被安装时osmp-service组件进行bundle监听时，通过bundle获取interface service 后解析type和 pattern并注入到ServiceContainer中。
 * 2、通过osmp-web在线可以绑定和解绑拦截器与bundle、服务。
 * 
 * @author: wangkaiping
 * @date: 2016年8月9日 上午9:26:32上午10:51:30
 */
public interface ServiceInterceptor {
    Object invock(ServiceInvocation invocation);
}
