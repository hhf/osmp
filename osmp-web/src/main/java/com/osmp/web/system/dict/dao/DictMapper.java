package com.osmp.web.system.dict.dao;

import com.osmp.web.core.mybatis.BaseMapper;
import com.osmp.web.system.dict.entity.Dict;

/**
 * Description:
 * 
 * @author: wangkaiping
 * @date: 2014年11月14日 下午2:28:49
 */
public interface DictMapper extends BaseMapper<Dict> {

}
